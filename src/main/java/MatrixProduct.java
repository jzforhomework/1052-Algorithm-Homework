
public class MatrixProduct {

    /**
     * 回傳 A * B，使用 Strassen's Algorithm
     *
     * @param A 矩陣 (m * n)
     * @param B 矩陣 (n * k)
     * @return C 為 A * B 的結果 (m * k)
     */
    public int[][] solve(int[][] A, int[][] B){
        int m=A.length,n=A[0].length,k=B[0].length;
        int count=0,b=1,i,j,n_size;
        if(m>n && m>k) {
            n_size=m;
        }
        else if(n>k&&n>m) {
            n_size=n;
        }
        else {
            n_size=k;
        }
        for(i=1;i<n_size;i++) {
            b*=2;
            if(b==n_size)
                break;
        }
        if(b==n_size) {
            n_size=b;
        }
        else {
            while (n_size != 0) {
                n_size = n_size / 2;
                count++;
            }
            n_size =  (int)Math.pow(2,count);
        }
        int [][] new_A = new int [n_size][n_size];
        int [][] new_B = new int [n_size][n_size];

        for( i=0;i<m;i++) {
            for( j=0;j<n;j++) {
                new_A[i][j] = A[i][j];
            }
        }
        for( i=0;i<n;i++) {
            for( j=0;j<k;j++) {
                new_B[i][j]=B[i][j];
            }
        }
        int[][] ans_C = stra(new_A,new_B);
        int[][] C = new int[m][k];
        for(i=0;i<m;i++) {
            for(j=0;j<k;j++) {
                C[i][j] = ans_C[i][j];
            }
        }
        return C;
    }

    public int[][]stra(int[][]A,int[][]B) {
        int n = A.length;
        int resize  = n/2;
        if(resize == 0) {
            return m_Matrix(A,B);
        }
        else {

            int[][] a11 = new int[resize][resize];
            int[][] a12 = new int[resize][resize];
            int[][] a21 = new int[resize][resize];
            int[][] a22 = new int[resize][resize];

            int[][] b11 = new int[resize][resize];
            int[][] b12 = new int[resize][resize];
            int[][] b21 = new int[resize][resize];
            int[][] b22 = new int[resize][resize];

            int[][] aResult ;
            int[][] bResult;

            for (int i = 0; i < resize; i++) {
                for (int j = 0; j < resize; j++) {
                    a11[i][j] = A[i][j];
                    a12[i][j] = A[i][j + resize];
                    a21[i][j] = A[i + resize][j];
                    a22[i][j] = A[i + resize][j + resize];

                    b11[i][j] = B[i][j];
                    b12[i][j] = B[i][j + resize];
                    b21[i][j] = B[i + resize][j];
                    b22[i][j] = B[i + resize][j + resize];
                }
            }
            // calculate P_1
            aResult = add(a11, a22);
            bResult = add(b11, b22);
            int[][] p_1 = m_Matrix(aResult, bResult);
            // calculate P_2
            aResult = add(a21, a22);
            int[][] p_2 = m_Matrix(aResult, b11);
            // calculate P_3
            bResult = sub(b12, b22);
            int[][] p_3 = m_Matrix(a11, bResult);
            // calculate P_4
            bResult = sub(b21, b11);
            int[][] p_4 = m_Matrix(a22, bResult);
            // calculate P_5
            aResult = add(a11, a12);
            int[][] p_5 =m_Matrix(aResult, b22);
            // calculate P_6
            aResult = sub(a21, a11);
            bResult = add(b11, b12);
            int[][] p_6 = m_Matrix(aResult, bResult);
            // calculate P_7
            aResult = sub(a12, a22);
            bResult = add(b21, b22);
            int[][] p_7 =m_Matrix(aResult, bResult);

            aResult = add(p_1, p_4);
            bResult = add(aResult, p_7);

            int[][] c11 = sub(bResult, p_5);

            int[][] c12 = add(p_3, p_5);

            int[][] c21 = add(p_2, p_4);

            aResult = add(p_1, p_3);
            bResult = add(aResult, p_6);
            int[][] c22 = sub(bResult, p_2);

            int[][] C = new int[n][n];
            for (int i = 0; i < resize; i++) {
                for (int j = 0; j < resize; j++) {
                    C[i][j] = c11[i][j];
                    C[i][j + resize] = c12[i][j];
                    C[i + resize][j] = c21[i][j];
                    C[i + resize][j + resize] = c22[i][j];
                }
            }
            return C;
        }
    }
    public static int[][] m_Matrix(int[][] A,int[][] B){
        int n =A.length;

        int[][] C = new int[n][n];

        for(int i = 0;i<n;i++) {
            for(int j=0;j<n;j++) {
                for(int k=0;k<n;k++) {
                    C[i][j]+=A[i][k]*B[k][j];
                }
            }
        }
        return C;
    }
    private static int[][] sub(int[][] A, int[][] B){
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] - B[i][j];
            }
        }
        return C;
    }
    private static int[][] add(int[][] A, int[][] B){
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        return C;
    }
}


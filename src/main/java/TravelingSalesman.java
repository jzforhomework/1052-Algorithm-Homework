import jdk.nashorn.internal.ir.EmptyNode;
import java.lang.Math;

public class TravelingSalesman {

    public int solve(int City, int[][] info) {
        int[][] W_matrix = new int[City][City];
        for (int i = 0 ; i < City ; i++){
            for (int j = 0 ; j < City ; j++){
                if( i==j )
                    W_matrix[i][j] = 0;
                else
                    W_matrix[i][j] = -1;
            }
        }
        for (int i = 0 ; i < info.length ; i++){
            W_matrix[ info[i][0] ][ info[i][1] ]= info[i][2];
        }
        int form_Column = (int) Math.pow(2,City-1);
        int form_Row = City;
        int[][] Final_Matrix = new int[City][form_Column];
        for (int i = 0 ; i < City ; i++){
            for(int j = 0 ; j < form_Column ; j++){
                Final_Matrix[i][j] = -1;
            }
        }
        for (int i = 0 ; i < City ; i++){
            Final_Matrix[i][0] = W_matrix[i][0];
        }
        for (int i = 1 ; i < form_Column-1 ; i++){
            for (int j = 1 ; j < City ; j++){
                if ( ( (1 << j-1) & i ) ==0 ){
                    int min = Integer.MAX_VALUE;
                    for( int k = 1 ; k < City ; k++ ){
                        if ( ( (1 << k-1) & i )!= 0 && W_matrix[j][k]!=-1 && Final_Matrix[k][i - (1 << k-1) ]!=-1) {
                            int temp = W_matrix[j][k] + Final_Matrix[k][i - (1 << k-1) ];
                            if (min > temp){
                                min = temp;
                                Final_Matrix[j][i] = min;
                            }
                        }
                    }
                }
            }
        }
        int min = Integer.MAX_VALUE;
        for (int i = 1 ; i < City ; i++){
            if(W_matrix[0][i]!=-1 && Final_Matrix[i][ form_Column-1 - (1 << i-1) ]!=-1){
                int temp = W_matrix[0][i] + Final_Matrix[i][ form_Column-1 - (1 << i-1) ];
                if ( min > temp ){
                    min = temp;
                    Final_Matrix[0][form_Column-1] = min;
                }
            }
        }
        return Final_Matrix[0][form_Column-1];
    }
}
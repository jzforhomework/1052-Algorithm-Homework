
public class WeightedAverage {
    //計算加權
    public double solve(int[] grades, int[] credit) {
        double avg,cTotal=0,total=0;
        for(int i=0;i<grades.length;i++){
            total = total + grades[i] * credit[i];
        }
        for(int j=0;j<credit.length;j++){
            cTotal = cTotal + credit[j] ;
        }
        avg = total / cTotal;
        return avg;
    }

}

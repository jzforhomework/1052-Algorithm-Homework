import java.util.Arrays;
//BinaryKnapsackWithBranchAndBound
import java.lang.reflect.Array;
import java.util.Arrays;

import static java.util.Arrays.*;

/**
 * 作業03A - 背包問題(Backtracking)
 */
public class BinaryKnapsackWithBranchAndBound {
    /**
     * 使用 Backtracking 演算法，求出背包可以攜帶最高的攻擊力總和
     *
     * @return 在背包耐重內，最高的寶石攻擊力總和
     */
    public class n{
        int n_atk=0;
        int topbest;
        int n_weight=0;
        int n_item=0;
        int n_up=0;
        int no_count=0;
        int no_arr[];



        public n(n a) {
            n_atk=a.n_atk;
            n_weight=a.n_weight;
            n_item=a.n_item;
            n_up=a.n_up;
            no_count=a.no_count;
            no_arr=a.no_arr;

        }

        public n() {

        }
    }
    public  n n1=new n();
    public int solve(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight) {

        n1.no_arr=new int[gemCount];
        float avg[]= new float[gemCount];
        for (int i = 0; i < gemCount; i++)
        {
            avg[i] = (float) gemsATK[i] / (float) gemsWeight[i];
        }

        for(int i=0;i<gemCount;i++)
        {
            for(int j=0;j<gemCount;j++)
            {
                if(avg[i]>avg[j])
                {
                    avg[j]= (float) swap(avg[i],avg[i]=avg[j]);
                    gemsATK[j]= (int) swap(gemsATK[i],gemsATK[i]=gemsATK[j]);
                    gemsWeight[j]= (int) swap(gemsWeight[i],gemsWeight[i]=gemsWeight[j]);
                }
            }
        }


        n1.topbest=upp( gemCount, gemsATK,  gemsWeight,  knapsackMaxWeight);
        return backtrack( gemCount, gemsATK,  gemsWeight,  knapsackMaxWeight);
    }
    int backtrack(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight)
    {

        int best1=0,best2=0;

        if(n1.n_item==gemCount||n1.n_weight==knapsackMaxWeight||n1.n_atk==n1.topbest)
        {
            return n1.n_atk;
        }
        if(n1.n_weight+gemsWeight[n1.n_item]<=knapsackMaxWeight)
        {
            n1.n_atk+=gemsATK[n1.n_item];
            n1.n_weight+=gemsWeight[n1.n_item];
            n1.n_item+=1;
            best1= backtrack( gemCount,  gemsATK,  gemsWeight,  knapsackMaxWeight);
            n1.n_item-=1;
            n1.n_atk=n1.n_atk-gemsATK[n1.n_item];
            n1.n_weight=n1.n_weight-gemsWeight[n1.n_item];
        }


        n1.n_up= upp( gemCount, gemsATK,  gemsWeight,  knapsackMaxWeight);

        if(best1<=n1.n_up)
        {
            n1.no_arr[n1.no_count]=n1.n_item;
            n1.no_count+=1;
            n1.n_item+=1;
            best2= backtrack( gemCount,  gemsATK,  gemsWeight,  knapsackMaxWeight);
            n1.no_count-=1;
            n1.n_item--;

            if(best2>best1)
                return best2;
            return best1;
        }

        return best1;
    }
    int upp(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight)
    {
        int flag=0;
        int w=0;
        n1.n_up=0;
        for(int i=0;i<gemCount;i++)
        {
            for(int j=0;j<n1.no_count;j++)
            {

                if(n1.no_arr[j]==i)
                {
                    flag=1;
                    break;
                }
            }
            if(flag==0)
            {
                if(w+gemsWeight[i]<=knapsackMaxWeight)
                {
                    w+=gemsWeight[i];
                    n1.n_up+=gemsATK[i];

                }
                else
                {
                    int less=knapsackMaxWeight-w;
                    n1.n_up+=less*(gemsATK[i]/gemsWeight[i]);

                    return  n1.n_up;
                }
            }
            else
            {

                flag=0;
            }
        }

        return n1.n_up;
    }
    Object swap(Object a,Object b)
    {
        return a;
    }
}

